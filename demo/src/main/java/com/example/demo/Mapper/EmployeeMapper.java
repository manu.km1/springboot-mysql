package com.example.demo.Mapper;

import com.example.demo.Dto.EmployeeDto;
import com.example.demo.Entity.Employee;

public class EmployeeMapper {
    public static EmployeeDto mapToEmployeeDto(com.example.demo.Entity.Employee employee){
        return new EmployeeDto(
                employee.getId(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getEmail()
        );
    }
    public static com.example.demo.Entity.Employee mapToEmployee(EmployeeDto employeeDto){
        return new Employee(
                employeeDto.getId(),
                employeeDto.getFirstName(),
                employeeDto.getLastName(),
                employeeDto.getEmail()
        );
    }
}
