package com.example.demo;

import com.example.demo.Entity.Employee;
import com.example.demo.Repository.EmployeeRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
/*

Retuen DB Test cases for Learning purpose.

* */
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DemoApplicationTests {
	@Autowired
	EmployeeRepository employeeRepository;

	// Test case for creating employee --> CREATE
	@Test
	@Order(1)
	public void testCasesForCreateEmployee(){
		Employee employee = new Employee();
		employee.setFirstName("Manu");
		employee.setLastName("KM");
		employee.setEmail("manukm969@gmail.com");
		employeeRepository.save(employee);

		assertNotNull(employeeRepository.findById(1L));
	}

	// Test case for checking all the employees. ---->READ ALL
	@Test
	@Order(2)
	public void testCasesForAllEmployee(){
		List<Employee> employees = employeeRepository.findAll();
		Assertions.assertThat(employees).size().isGreaterThan(0);
	}

	// Test case for checking single employees.---> READ ONE
	@Test
	@Order(3)
	public void testCasesForSingleEmployee(){
		Employee employee = employeeRepository.findById(1L).get();
		assertEquals("Manu", employee.getFirstName());
	}

	// Test case for Update employee. ---> UPDATE
	@Test
	@Order(4)
	public void testCasesForUpdateEmployee(){
		Employee employee = employeeRepository.findById(1L).get();
		employee.setId(1l);
		employee.setFirstName("Shelby");
		employeeRepository.save(employee);
		assertNotEquals("Manu", employeeRepository.findById(1L));
	}

//	 Test case for Update employee. ---> DELETE
	@Test
	@Order(5)
	public void testCasesForDeleteEmployee(){
		employeeRepository.deleteById(1L);
		Assertions.assertThat(employeeRepository.existsById(1L)).isFalse();
	}
}