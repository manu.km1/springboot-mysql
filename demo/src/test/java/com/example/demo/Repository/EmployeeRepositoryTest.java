package com.example.demo.Repository;

import com.example.demo.Entity.Employee;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void EmployeeRepository_SavedEmployee_ReturnSavedEmployee(){
        // Arrange
        Employee employee = Employee.builder().firstName("Manu").lastName("KM").email("manukm@gmail.com").build();
        // Act
        Employee savedEmployee = employeeRepository.save(employee);
        // Assert
        Assertions.assertThat(savedEmployee).isNotNull();
        Assertions.assertThat(savedEmployee.getId()).isGreaterThan(0L);
        Assertions.assertThat(savedEmployee.getFirstName()).isEqualTo(employee.getFirstName());
        Assertions.assertThat(savedEmployee.getLastName()).isEqualTo(employee.getLastName());
        Assertions.assertThat(savedEmployee.getEmail()).isEqualTo(employee.getEmail());
    }
    @Test
    public void EmployeeRepository_FindById_ReturnEmployee(){
        // Arrange
        Employee employee = Employee.builder().firstName("Manu").lastName("KM").email("manukm@gmail.com").build();
        employeeRepository.save(employee);
        // Act
        Employee returnedEmployee = employeeRepository.findById(employee.getId()).get();
        // Assert
        Assertions.assertThat(returnedEmployee).isNotNull();
    }
    @Test
    public void EmployeeRepository_GetAll_RetuensMoreThenOneEmployee(){
        // Arrange
        Employee employee1 = Employee.builder().firstName("Manu").lastName("KM").email("manukm@gmail.com").build();
        Employee employee2 = Employee.builder().firstName("Ragnar").lastName("Lothbrok").email("ragnar@gmail.com").build();
        Employee employee3 = Employee.builder().firstName("Thomas").lastName("Shelby").email("shelby@gmail.com").build();
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        // Act
        List<Employee> employeeList = employeeRepository.findAll();
        // Assert
        Assertions.assertThat(employeeList).isNotNull();
        Assertions.assertThat(employeeList.size()).isEqualTo(3);
    }
    @Test
    public void EmployeeRepository_UpdateEmployee_ReturnUpdatedEmployee(){
        // Arrange
        Employee employee = Employee.builder().firstName("Manu").lastName("KM").email("manukm@gmail.com").build();
        employeeRepository.save(employee);
        // Act
        Employee savedEmployee = employeeRepository.findById(employee.getId()).get();
            savedEmployee.setFirstName("Anu");
            savedEmployee.setLastName("Tanu");
            savedEmployee.setEmail("anutanu@gmail.com");
        Employee updatedEmployee = employeeRepository.save(savedEmployee);
        // Assert
        Assertions.assertThat(updatedEmployee.getFirstName()).isNotNull();
        Assertions.assertThat(updatedEmployee.getLastName()).isNotNull();
        Assertions.assertThat(updatedEmployee.getEmail()).isNotNull();
    }
    @Test
    public void EmployeeRepository_EmployeeDelete_RetuenEmployeeIsEmpty(){
        // Arrange
        Employee employee = Employee.builder().firstName("Manu").lastName("KM").email("manukm@gmail.com").build();
        employeeRepository.save(employee);
        // Act
        employeeRepository.deleteById(employee.getId());
        Optional<Employee> employeeReturn = employeeRepository.findById(employee.getId());
        // Assert
        Assertions.assertThat(employeeReturn).isEmpty();
    }
}